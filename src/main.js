import { createApp } from 'vue'
import Header from './Header.vue'
import './assets/css/tailwind.css'
import './assets/css/main.scss'

createApp(Header).mount('#app')
